//
//  FlashcardTableViewCell.swift
//  flashcards
//
//  Created by Jonathan Perz on 2/11/21.
//
import Foundation
import UIKit

class FlashcardTableViewCell: UITableViewCell {
    @IBOutlet var flashcardLabel: UILabel!
}
