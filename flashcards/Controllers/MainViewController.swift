//
//  ViewController.swift
//  flashcards
//
//  Created by Jonathan Perz on 2/3/21.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    @IBOutlet var collectionView: UICollectionView!
    var sets: [FlashcardSets] = []
    var selectedIndex: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let setClass = FlashcardSet()
//        setsStatic = setClass.getHardCodedCollection()

        collectionView.delegate = self
        collectionView.dataSource = self
        getFlashcardSetsFromDB ()
    }
    
    func getFlashcardSetsFromDB () {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FlashcardSets")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        do {
            sets = try managedContext.fetch(fetchRequest) as? [FlashcardSets] ?? []
            collectionView.reloadData()
        } catch let error as NSError {
            print ("Could not fetch. \(error), \(error.userInfo)")
        }
    }

    @IBAction func addNewSet(_ sender: Any) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return }

        let managedContext = appDelegate.persistentContainer.viewContext

        let entity = NSEntityDescription.entity(forEntityName: "FlashcardSets",
                                  in: managedContext)!

        let set = NSManagedObject(entity: entity, insertInto: managedContext) as! FlashcardSets
        set.title = "Title \(sets.count+1)"
        
        do {
            try managedContext.save()
            sets.append(set)
            collectionView.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
//        let newSet = FlashcardSets()
//        newSet.title = "Title \(sets.count+1)"
//        sets.append(newSet)
//        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        cell.textLabel.text = sets[indexPath.row].title
        cell.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = collectionView.cellForItem(at: indexPath)?.tag ?? 0
        print (selectedIndex)
        performSegue(withIdentifier: "GoToDetail", sender: self)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDetail" {
            let VC = segue.destination as! FlashcardSetDetailViewController
            VC.selectedSet = sets[selectedIndex]
            print(VC.selectedSet?.title)
        }
    }
    
}
