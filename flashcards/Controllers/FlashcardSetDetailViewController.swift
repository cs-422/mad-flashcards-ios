//
//  FlashcardSetDetailViewController.swift
//  flashcards
//
//  Created by Jonathan Perz on 2/11/21.
//

import UIKit
import CoreData

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!

    @IBOutlet weak var FlashcardSetLabel: UILabel!
    var selectedIndex: Int = 0
    var selectedSet: FlashcardSets?
    var cards: [Flashcards] = []
    
//    @IBOutlet var buttonView: UIView!

    @IBOutlet weak var flashcardAlertView: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let flashClass = Flashcard()
//        cards = flashClass.getHardCodedCollection()
        setup()
    }
        
        func setup() {
            tableView.delegate = self
            tableView.dataSource = self
            
            if let set = selectedSet {
                FlashcardSetLabel.text = set.title
                cards = Array(set.cards as! Set<Flashcards>)
                cards.sort{$0.term ?? "" < $1.term ?? ""}
                tableView.reloadData()
            }
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        self.tableView.addGestureRecognizer(longPressGesture)
    }
    
    @IBAction func addCard(_ sender: Any) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return }

        let managedContext = appDelegate.persistentContainer.viewContext

        let entity = NSEntityDescription.entity(forEntityName: "Flashcards",
                                  in: managedContext)!

        let card = NSManagedObject(entity: entity, insertInto: managedContext) as! Flashcards
        card.term = "Term \(cards.count+1)"
        card.definition = "Definition \(cards.count+1)"
        card.set = selectedSet
        
        do {
            selectedSet?.addToCards(card)
            try managedContext.save()
            cards = Array(selectedSet?.cards as! Set<Flashcards>)
            cards.sort{$0.term ?? "" < $1.term ?? ""}
            tableView.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
//        let newCard = Flashcard()
//        newCard.term = "Term \(cards.count+1)"
//        newCard.definition = "Definition \(cards.count+1)"
//        cards.append(newCard)
//        tableView.reloadData()
    }
    
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        //find out where the long press is
        let p = longPressGesture.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        //if long press is starting (this will also trigger on ending if you dont have this if statement)
        if longPressGesture.state == UIGestureRecognizer.State.began {
            selectedIndex = indexPath?.row ?? 0
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let alertVC = sb.instantiateViewController(identifier: "CustomAlertViewController") as! CustomAlertViewController
            alertVC.parentVC = self
            alertVC.modalPresentationStyle = .overCurrentContext
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: {})
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        return cell 
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let deletedCard = cards[indexPath.row]
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            managedContext.delete(deletedCard)
            selectedSet?.removeFromCards(deletedCard)
            cards.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    @IBAction func btn_Delete(_ sender: Any) {
        self.dismiss(animated: true, completion: {})
    }
    
    func createAlert(indexPath: IndexPath)
    {
        let alert = UIAlertController(title: "\(cards[indexPath.row].term)", message: "\(cards[indexPath.row].definition)", preferredStyle: .alert)
        selectedIndex = indexPath.row
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            alert.dismiss(animated: true, completion: {})
            self.createCustomEditAlert()
        }))
        self.present(alert, animated: true)
    }
    
    //create custom alert
    func createCustomEditAlert()
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "CustomAlertViewController") as! CustomAlertViewController
        alertVC.parentVC = self
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        createAlert(indexPath: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToStudy" {
            let VC = segue.destination as! StudySetViewController
            VC.cards = cards
        }
    }
}
