//
//  CustomAlertViewController.swift
//  flashcards
//
//  Created by Jonathan Perz on 2/18/21.
//
import UIKit

class CustomAlertViewController: UIViewController {
    
    @IBOutlet var alertView: UIView!
    var parentVC: FlashcardSetDetailViewController!
    @IBOutlet var term: UITextField!
    @IBOutlet var definition: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToLookPretty()
        //set text and description of dialog
        term.text = parentVC.cards[parentVC.selectedIndex].term
        definition.text = parentVC.cards[parentVC.selectedIndex].definition
    }
    
    @IBAction func save(_ sender: Any) {
        parentVC.cards[parentVC.selectedIndex].term = term.text ?? ""
        parentVC.cards[parentVC.selectedIndex].definition = definition.text ?? ""
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    @IBAction func deleteItem(_ sender: Any) {
        parentVC.cards.remove(at: parentVC.selectedIndex)
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }

    
    func setupToLookPretty()
    {
        alertView.layer.cornerRadius = 8.0
        alertView.layer.borderWidth = 3.0
        alertView.layer.borderColor = UIColor.gray.cgColor
        term.becomeFirstResponder()
    }
    
}

