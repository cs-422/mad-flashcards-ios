//
//  StudySetViewController.swift
//  flashcards
//
//  Created by Jonathan Perz on 2/18/21.
//

import UIKit

class StudySetViewController: UIViewController {
    
    @IBOutlet var card: UIView!
    @IBOutlet var Item: UILabel!
    @IBOutlet weak var labelCompleted: UILabel!
    @IBOutlet weak var labelCorrect: UILabel!
    @IBOutlet weak var labelMissed: UILabel!
    var cards: [Flashcards] = [Flashcards]()
    var completeNum = 0
    var missedNum = 0
    var correctNum = 0
    var cardSize = 0
    var missedList = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupToLookPretty()
//        let setClass = Flashcard()
//        cards = setClass.getHardCodedCollection()
        Item.text = cards.first?.term
        cardSize = cards.count
        labelMissed.text = "Missed:  \(missedNum)"
        labelCorrect.text = "Correct:  \(correctNum)"
        labelCompleted.text = "Complete: \(completeNum)/" + "\(cardSize)"
        
    }
    
    @IBAction func tapItem(_ sender: Any) {
        Item.text = cards.first?.definition
        Item.reloadInputViews()
    }
    
    @IBAction func missedButton(_ sender: Any) {
        labelMissed.text = "Missed:  \(missedNum)"
        missedList.append(cards.first!.term ?? "")
        missedNum = missedNum + 1
        let element = cards.removeFirst()
        cards.append(element)
        updateLabels()
    }

    @IBAction func skipButton(_ sender: Any) {
        let element = cards.removeFirst()
        cards.append(element)
        updateLabels()
    }
    
    @IBAction func correctButton(_ sender: Any) {
        if missedList.contains(cards.first!.term ?? "") {
            let ML = false
        } else {
            correctNum = correctNum + 1
        }
        completeNum = completeNum + 1
        updateLabels()
        cards.removeFirst()
        
        if completeNum < cardSize {
            updateLabels()
        } else {
            createEndAlert()
        }
        
    }
    
    @IBAction func exitButton(_ sender: Any) {
        self.dismiss(animated: true, completion: {})
    }
    
    func updateLabels() {
        Item.text = cards.first?.term
        labelMissed.text = "Missed:  \(missedNum)"
        labelCorrect.text = "Correct:  \(correctNum)"
        labelCompleted.text = "Complete: \(completeNum)/" + "\(cardSize)"
    }
    
    func createEndAlert()
    {
        let alert = UIAlertController(title: "Congratulations!", message: "You finished this set!", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
                                      self.dismiss(animated: true, completion: {})
                                      }))

        self.present(alert, animated: true)
    }
    
    func setupToLookPretty()
    {
        card.layer.cornerRadius = 8.0
        card.layer.borderWidth = 3.0
        card.layer.borderColor = UIColor.gray.cgColor
        Item.becomeFirstResponder()
    }

}
